import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  constructor( private router: Router ) { }

  ngOnInit() {
  }

  addExpensesFunction(event) {
    this.router.navigate(['/expenseformpage'])
    .catch(console.error);
  }

  addIncomeFunction(event) {
    this.router.navigate(['/incomeformpage'])
    .catch(console.error);
  }

}
