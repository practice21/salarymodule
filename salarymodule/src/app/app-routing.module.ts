import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { ReportpageComponent } from './reportpage/reportpage.component';
import { ExpensesFormPageComponent } from './expenses-form-page/expenses-form-page.component';
import { IncomeFormPageComponent } from './income-form-page/income-form-page.component';



const routes: Routes = [
  { path:'home', component: HomepageComponent },
  { path: 'report', component: ReportpageComponent },
  { path: 'expenseformpage', component: ExpensesFormPageComponent },
  { path: 'incomeformpage', component: IncomeFormPageComponent }
  // { path: '', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
